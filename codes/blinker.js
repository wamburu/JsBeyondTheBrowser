// Blink an LED
var five = require("johnny-five");
var board = new five.Board();

board.on("ready", function() {
    //create an LED on pin 13
  var led = new five.Led(13);
  
    //strobe the pin on/off, default is 100ms
    led.blink();
});